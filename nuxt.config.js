export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'EUMTS',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  mode: 'spa',

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'assets/css/nucleo/css/nucleo.css',
    'assets/sass/argon.scss'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/dashboard-plugin',
    '~/plugins/click-outside',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    [
      '@nuxtjs/firebase',
      {
        config: {
          apiKey: "AIzaSyAt1BYMvqhT0kTZAm4KagJm9YejjHbUaJA",
          authDomain: "humber-c17ca.firebaseapp.com",
          projectId: "humber-c17ca",
          storageBucket: "humber-c17ca.appspot.com",
          messagingSenderId: "481154599500",
          appId: "1:481154599500:web:6c7c074bb909234b7d82e9",
          measurementId: "G-MFX3DNWSX8"
        },
        services: {
          auth: true, // Just as example. Can be any other service.,
          firestore: true,
        }
      }
    ]
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [
      'vee-validate/dist/rules'
    ],
  }
}
