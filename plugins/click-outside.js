import clickOutside from 'vue-click-outside';
import Vue from 'vue';

Vue.directive('click-outside', clickOutside);
