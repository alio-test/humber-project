export const state = () => ({
	products: [],
})
export const actions = {
	getProducts({ commit }) {
		this.$fire.firestore
			.collection("products")
			.get()
			.then((querySnapshot) => {
				commit('setProducts', querySnapshot.docs)
			});
	},
	changeProductStatus({ commit }, { product, status }) {
		this.$fire.firestore
			.collection("products")
			.doc(product.row.id)
			.set({
				date: new Date(product.row.date),
				name: product.row.name,
				status: status,
			})
			.then(() => {
				console.log("Document successfully written!");
			})
			.catch((error) => {
				console.error("Error writing document: ", error);
			});
	},
}
export const mutations = {
	// const date = new Date().toLocaleString("en-GB", {
	// 	day: "numeric",
	// 	month: "numeric",
	// 	year: "numeric",
	// })
	// .split("/")
	// .map((item) => item.padStart(2, "0"))
	// .join("/");
	setProducts(state, products) {
		state.products = products.map((product) => ({
			...product.data(),
			date: new Date(product.data().date.seconds * 1000).toLocaleString("en-US", {
				day: "numeric",
				month: "numeric",
				year: "numeric",
			})
				.split("/")
				.map((item) => item.padStart(2, "0"))
				.join("/"),
			id: product.id,
		}));
	},
}
export const getters = {
	products: s => s.products,
}	