export const state = () => ({
	user: {},
})
export const actions = {
	getUser({ commit }) {
		this.$fire.auth.onAuthStateChanged((user) => {
			if (user) {
				let docRef = this.$fire.firestore
					.collection("users")
					.doc(user.uid);
				docRef
					.get()
					.then((doc) => {
						if (doc.exists) {
							commit("setUserData", doc.data());
						} else {
							// doc.data() will be undefined in this case
							console.log("No such document!");
						}
					})
					.catch((error) => {
						console.log("Error getting document:", error);
					});
			} else {
				this.$router.push("/")
			}
		});
	},
}
export const mutations = {
	setUserData(state, user) {
		state.user = user;
		// console.log(state.user)
	},
}
export const getters = {
	user: s => s.user,
}